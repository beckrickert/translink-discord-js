#!/bin/sh

echo "Creating ${MYSQL_USER} db user"

mysql -e "CREATE USER '${MYSQL_USER}'@'${HOSTNAME}' IDENTIFIED BY '${MYSQL_PASSWORD}'" -u root --password='${MYSQL_ROOT_PASSWORD}' -h '${MYSQL_HOST}'

echo "Setting appropriate database permissions on ${MYSQL_USER}@${HOSTNAME}"

mysql -e "REVOKE ALL PRIVILEGES ON '${MYSQL_DATABASE}'.'*' FROM ${MYSQL_USER}'@'%'" -u root --password='${MYSQL_ROOT_PASSWORD}' -h '${MYSQL_HOST}'

mysql -e "GRANT SELECT, INSERT, DELETE, UPDATE ON '${MYSQL_DATABASE}'.'*' TO ${MYSQL_USER}'@'%'" -u root --password='${MYSQL_ROOT_PASSWORD}' -h '${MYSQL_HOST}'