#!/bin/sh

echo "Setting system timezone to ${TZ}"

apk add tzdata

cp /usr/share/zoneinfo/${TZ} /etc/localtime

echo "${TZ}" > /etc/localtime

apk del tzdata