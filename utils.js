var fs = require('fs');
var path = require('path');
var imaps = require('imap-simple');
var _ = require('lodash');
var translink = require('./translink.js');

module.exports = {
    removeAuthorMsg(message) {
        if (message.channel.type != 'dm') {
            message.author.lastMessage.delete({timeout: 1000});
        }
    },

    aboutMessage() {
        return fs.readFileSync(path.join(__dirname, 'assets', 'about.txt'), 'utf8').replace(/\r/g, "").trim();
    }
}