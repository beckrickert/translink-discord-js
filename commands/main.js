var utils = require('../utils.js');
module.exports = {
    name: 'translink',
    args: false,
    usage: '[No Arguments]',
    guildOnly: false,
    cooldown: 5,
    aliases: ["bloofare", "bf"],
    description: "About command for Bloofare bot",
    execute(message, args) {
        var txtArr = utils.aboutMessage();
        txtArr = txtArr.replace(/%USERID%/g, message.author.username);
        txtArr = txtArr.replace(/%PREFIX%/g, process.env.PREFIX);
        txtArr = txtArr.replace(/%OWNERTAG%/g, process.env.OWNER_TAG);
        txtArr = txtArr.split("\n");

        utils.removeAuthorMsg(message);
        //dm the response
        return message.author.send(txtArr, {split: true})
        .then(() => {
            if (message.channel.type === 'dm') return;
        })
        .catch((err) => {
            message.reply("Couldn't send your request to your DMs. You might have DMs disabled.");
        });
    }
}