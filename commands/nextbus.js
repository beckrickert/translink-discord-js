var translink = require('../translink.js');
var utils = require('../utils.js');
module.exports = {
    name: 'nb',
    args: true,
    usage: '[stop number] [route number]',
    guildOnly: false,
    cooldown: 5,
    aliases: ["nextbus"],
    description: 'Get the next 3 buses for each bus route and per destination',
    execute(message, args) {
        var stopNo = args[0];
        var routeNo = args[1];

        var msg = [];

        translink.nextBus(stopNo, routeNo, (data) => {
            if (!data.error) {
                msg.push(`>>> ${data.routeNo} ${data.routeName}`);
                
                data.schedules.forEach(s => {
                    msg.push(`To ${s.to} \`${s.time}\` ${(s.status != "SCHEDULED") ? s.status : ""}  ${(s.routeCancelled) ? "Route \`Cancelled\`" : ""} ${(s.stopCancelled) ? "Stop \`Cancelled\`" : ""}`);
                });

                //clean up
                msg = msg.join("\n").trim().replace(/ +/g, " ");

                utils.removeAuthorMsg(message);

                return message.channel.send(msg, { split: true });
            }
            else {
                if (data.error) {
                    utils.removeAuthorMsg(message);
                    return message.channel.send(data.error.Message, { split: true });
                }
            }
        });
        
    }
};