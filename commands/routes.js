var translink = require('../translink.js');
var utils = require('../utils.js');
module.exports = {
    name: 'routes',
    args: true,
    usage: '[stop number]',
    guildOnly: false,
    cooldown: 5,
    aliases: ["buses"],
    description: 'List the routes for a stop',
    execute(message, args) {
        var stopNo = args[0];

        var msg = [];

        translink.routes(stopNo, (data) => {
            if (!data.error) {
                msg.push(`>>> Name: ${data.stopName} Buses: \`${data.buses}\` ${data.gMaps}`);

                //clean up
                msg = msg.join("\n").trim().replace(/ +/g, " ");

                utils.removeAuthorMsg(message);

                return message.channel.send(msg, { split: true });
            }
            else {
                if (data.Code) {
                    utils.removeAuthorMsg(message);
                    return message.channel.send(data.Message, { split: true });
                }
            }
        });
        
    }
};