var translink = require('../translink.js');
var utils = require('../utils.js');
module.exports = {
    name: 'alert',
    args: true,
    usage: '[route number] [optional: remove, rm, del, delete]',
    guildOnly: true,
    cooldown: 5,
    aliases: ["alertme", "routealerts"],
    description: 'Adds a route role to you to be mentioned of this specific route.',
    execute(message, args) {
        utils.removeAuthorMsg(message);

        
        var routeNo = args[0].toString().toUpperCase();
        var guild = message.guild;
        var user = message.author;
        var userGM = guild.members.cache.find(gm => gm.id == user.id);
        
        //check if routeNo exists as a route
        var isRouteNo = translink.routeNames.includes(routeNo);

        if (args[1] != undefined) {
            //remove user from route role
            var removeArg = args[1].toString().toUpperCase();
            if (['REMOVE', 'RM', 'DEL', 'DELETE'].includes(removeArg)) {
                
                //check if routeNo exists
                if (isRouteNo) {
                    //remove user from the role
                    try {
                        var routeRole = userGM.roles.cache.find(rl => rl.name.includes(routeNo));
                        if (routeRole != undefined) {

                            console.log(`Removed route role ${routeRole.name} ${routeRole.id} from user ${userGM.id} on command request`);

                            userGM.roles.remove(routeRole);
                            
                            return message.reply(`Removed the route role ${routeNo} from you. You will not be notified of these alerts anymore.`);

                        }
                    }
                    catch (e) {
                        console.log(e);
                        return message.reply('There was an error while trying to remove you from that route role.');
                    }
                }
                else {
                    return message.reply('That route number does not exist in Translink.');
                }


            }
            else {
                return message.reply(`That argument ${args[1]} does not exist, try "remove" instead.`);
            }
        }
        else {
            //add user to route role
            //check if routeNo exists as a route
            if (isRouteNo) {
                console.log(`Alert route provided is a valid route number.`);
                //check if route role exists
                //check if route channel exists
                var routeRole = guild.roles.cache.find(rl => rl.name.includes(routeNo));
                var routeChnl = guild.channels.cache.find(ch => ch.name.includes(`${routeNo}_route_alerts`));

                try {
                    if (routeRole != undefined) {
                        //add this user the role
                        userGM.roles.add(routeRole, "User wanted to be alerted to this specific route role by using the command.");
                        console.log(`Added userGM ${userGM.id} to route role ${routeRole.id} in guild ${guild.id}`);
                    }
                    else if (routeRole == undefined) {
                        //create routeRole and add user
                        var msg = {};
                        msg.embeds = [];
                        msg.embeds.push({
                            title: routeNo
                        });

                        translink.roles.addAndGetRouteRole(msg, guild, (err, res) => {
                            if (!err) {
                                userGM.roles.add(res.role, "User wanted to be alerted to this specific route role by using the command.");

                                console.log(`Added guild member ${userGM.id} at guild ${userGM.guild.id} with role ${res.role.id}`);
                                
                                if (routeChnl == undefined) {
                                    //create route channel under category
                                    var categoryChnl = guild.channels.cache.find(ch => ch.type == "category" && ch.name.includes("Translink Alert"));
                                    
                                    if (categoryChnl == undefined) {
                                        //create category channel
                                        translink.channels.createRootCategory(guild, (err, res) => {
                                            if (!err) {
                                                categoryChnl = res;
                                                //create route channel
                                                translink.channels.createRouteTextChannel(guild, res.role, (err, res) => {
                                                    if (err) {
                                                        console.log(err);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        //create route channel
                                        translink.channels.createRouteTextChannel(guild, res.role, (err, res) => {
                                            if (err) {
                                                console.log(err);
                                            }
                                        });
                                    }
                                }
                            }
                        });

                    }

                }
                catch (e) {
                    console.log(e);
                    return message.reply('There was an error while trying to assign you that route.');
                }
            }
            else {
                return message.reply('That route number does not exist in Translink.');
            }
        }
    }
};