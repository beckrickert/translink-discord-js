var translink = require('../translink.js');
var utils = require('../utils.js');
module.exports = {
    name: 'upass',
    args: true, 
    usage: 'option[add, remind, remindme, delete, remove, del, rm] date[early (default), end, last](optional)',
    guildOnly: false,
    cooldown: 10,
    aliases: ["multipass"],
    description: "Add or remove a reminder that is sent to you as a DM for UPass renewals either every 16th or last day of the month.",
    execute(message, args) {
        var option = args[0].toLowerCase();
        var date = "";
        if (args[1] != undefined) {
            date = args[1].toLowerCase();
        }

        var uid = message.author.id;

        utils.removeAuthorMsg(message);

        if (["add", "remind", "remindme", "delete", "remove", "del", "rm"].includes(option) 
            && ["early", "end", "last", ""].includes(date)) {
            switch (option) {
                case "add":
                case "remind":
                case "remindme":
                    if (date.length == 0) {
                        date = "early";
                    }

                    if (date == "end") {
                        date = "last";
                    }

                    translink.upass.check(uid)
                    .then((res) => {
                        if (res) {
                            //update reminder
                            translink.upass.update(uid, date, (err, r) => {
                                if (err) {
                                    console.log(err);
                                    //dm error
                                    message.author.send(err, {split: true})
                                    .catch(() => {
                                        //fallback to replying error
                                        message.reply(err);
                                    });
                                }
                                else {
                                    if (r) {
                                        message.author.send(`Your UPass Reminder has been updated. You will be reminded ${(["early"].includes(date)) ? "every 16th of the month." : "last day of each month."}`)
                                        .catch(() => {
                                            message.reply(`Your UPass Reminder has been updated. You will be reminded ${(["early"].includes(date)) ? "every 16th of the month." : "last day of each month."}`);
                                        });
                                    }
                                }
                            });
                        }
                        else {
                            //add reminder
                            translink.upass.add(uid, date, (err, r) => {
                                if (err) {
                                    console.log(err);
                                    //dm error
                                    message.author.send(err, {split: true})
                                    .catch(() => {
                                        //fallback to replying error
                                        message.reply(err);
                                    });
                                }
                                else {
                                    if (r) {
                                        message.author.send(`Your UPass Reminder has been added. You will be reminded ${(["early"].includes(date)) ? "every 16th of the month." : "last day of each month."}`)
                                        .catch(() => {
                                            message.reply(`Your UPass Reminder has been added. You will be reminded ${(["early"].includes(date)) ? "every 16th of the month." : "last day of each month."}`);
                                        });
                                    }
                                }
                            });
                        }
                    }).catch((err) => {
                        console.log(err);
                    });
                    break;
                case "delete":
                case "remove":
                case "del":
                case "rm":
                    translink.upass.check(uid)
                    .then((res) => {
                        if (res) {
                            //remove reminder
                            translink.upass.remove(uid, (err, r) => {
                                if (err) {
                                    //dm error
                                    message.author.send(err, {split: true})
                                    .catch(() => {
                                        //fallback to replying error
                                        message.reply(err);
                                    });
                                }
                                else {
                                    if (r) {
                                        message.author.send("Your UPass Reminder has been removed.")
                                        .catch(() => {
                                            message.reply("Your UPass Reminder has been removed.");
                                        });
                                    }
                                }
                            });
                        }
                        else {
                            //nothing to remove
                            message.author.send("There is no reminder to be removed.")
                            .catch(() => {
                                message.reply("There is no reminder to be removed.");
                            });
                        }
                    }).catch((err) => {
                        console.log(err);
                    });
                    break;
            }
        }
        else {
            message.author.send("option parameter must be either of `add, remind, remindme, delete, remove, del, rm` and applicably with date parameter `early, end, last`.", {split: true});
        }
    }
}