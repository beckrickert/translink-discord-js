var CronJob = require('cron').CronJob;
var momenttz = require('moment-timezone');
var today = momenttz(Date().now).tz(process.env.TZ);
var translink = require('./translink.js');
var imaps = require('imap-simple');

module.exports = {
    DayOfMonth16Jobs: (_client) => {
        return new CronJob('00 00 00 16 * *', () => {
            translink.upass.triggerReminders(_client, "early", (err, res) => {
                if (err) { console.log(err); }
            });
        }, null, true, process.env.TZ);
    },
    DailyMidnightJobs: (_client) => {
        return new CronJob('00 00 00 * * *', () => {
            //this is checked daily
            //if next day's month is not the same then today is the last day of the month
            if (today.month() != today.add(1, 'day').month()) {
                translink.upass.triggerReminders(_client, "last", (err, res) => {
                    if (err) { console.log(err); }
                });
            }
        }, null, true, process.env.TZ);
    },
    EveryFiveMinutesJobs: (_client) => {
        return new CronJob('00 */5 * * * *', () => {

            imaps.connect(translink.imap.config).then(connection => {
                console.log("IMAP Connected.");
                connection.openBox(translink.imap.inbox).then(() => {
                    console.log("Opened Translink mailbox.");
                    var searchCriteria = [
                        'UNSEEN',
                        //['FROM', 'donotreply@alerts.translink.ca']
                    ];
                    
                    var fetchOptions = {
                        bodies: ['HEADER', 'TEXT'],
                        markSeen: true
                    }
                    console.log("Searching messages.");
                    connection.search(searchCriteria, fetchOptions).then(messages => {
                        console.log(messages);
                        //filter messages only from alerts.translink.ca
                        messages = messages.filter(msg => msg.parts[0].body.from[0].includes('@alerts.translink.ca'));
                        if (messages.length != 0) {
                            console.log("Found new messages.");
                            var embeds = translink.imap.processMessages(messages);

                            //get all guilds that have translink category channel
                            var allGuilds = _client.guilds.cache;
                            var category_ch_guilds = allGuilds.filter(g => g.channels.cache.find(ch => ch.name.includes("Translink Alerts") && ch.type == "category") != undefined);

                            category_ch_guilds.forEach(g => {
                                embeds.forEach(embed => {
                                    //get route number by embed subject
                                    var routeNum = embed.title.split(' ')[0].trim().toUpperCase();

                                    //check if route specific channel exists
                                    //check if route specific role exists
                                    var routeRole = g.roles.cache.find(rl => rl.name.includes(routeNum));
                                    var routeChnl = g.channels.cache.find(ch => ch.name.includes(`${routeNum}_route_alerts`));
                                    
                                    //if yes to both, send to route channel
                                    if (routeRole != undefined || routeChnl != undefined) {
                                        console.log(`Sent an alert to route channel ${routeChnl.id} with mentioning role ${routeRole.id}`);
                                        
                                        //add mention
                                        //send alert to route channel
                                        routeChnl.send(`<@&${routeRole.id}>`, embed).then(msg => {

                                            //create reaction collector only if alert was a route alert (not a news feed)
                                            var msgTitle = msg.embeds[0].title.split(' ')[0];
                                            var isRouteMsg = translink.routeNames.includes(msgTitle);
                                            
                                            if (isRouteMsg) {
                                                var time = 600000;
                                                console.log(`Creating reaction collector for msg ${msg.id} lasting ${time} ms.`);

                                                //filter reaction emojis to buses
                                                var filter = (reaction) => reaction.emoji.name === '🚌';
                                                //collect reactions for 10minutes when since this message was created
                                                var reactionCollector = msg.createReactionCollector(filter, {
                                                    time: time, //10 minutes
                                                });

                                                reactionCollector.on('collect', (reaction, user) => {
                                                    
                                                    console.log(`Captured reaction event on message ${reaction.message.id} made by ${user.id}`);

                                                    var rMsg = reaction.message;

                                                    translink.roles.addAndGetRouteRole(rMsg, rMsg.channel.guild, (err, res) => {
                                                        if (!err) {
                                                            var userGM = msg.guild.members.cache.find(gm => gm.id == user.id);
                                                            userGM.roles.add(res.role, "User wanted to be alerted to this specific route role.");

                                                            console.log(`Added guild member ${userGM.id} at guild ${userGM.guild.id} with role ${res.role.id}`);

                                                        }
                                                        else {
                                                            console.log(res);
                                                        }
                                                    });
                                                });
                                            }

                                        }).catch(e => console.log(e));
                                    }
                                    else {
                                        //if not both, send to unfiltered alerts with no mention as fallback
                                        var unfiltered_chnl = g.channels.cache.find(ch => ch.name.includes("unfiltered_alerts"));
                                        if (unfiltered_chnl != undefined) {
                                            //send to unfiltered channel
                                            console.log(`Sent an alert to unfiltered channel ${unfiltered_chnl.id}`);
                                            
                                            unfiltered_chnl.send("", embed).then(msg => {

                                                //create reaction collector only if alert was a route alert (not a news feed)
                                                var msgTitle = msg.embeds[0].title.split(' ')[0];
                                                var isRouteMsg = translink.routeNames.includes(msgTitle);

                                                if (isRouteMsg) {
                                                    var time = 600000;
                                                    console.log(`Creating reaction collector for msg ${msg.id} lasting ${time} ms.`);

                                                    console.log(`Route role or route channel doesn't exist`);
    
                                                    //filter reaction emojis to buses
                                                    var filter = (reaction) => reaction.emoji.name === '🚌';
                                                    //collect reactions for 10minutes when since this message was created
                                                    var reactionCollector = msg.createReactionCollector(filter, {
                                                        time: time, //10 minutes
                                                    });
    
                                                    reactionCollector.on('collect', (reaction, user) => {
                                                        
                                                        console.log(`Captured reaction event on message ${reaction.message.id} made by ${user.id}`);
    
                                                        var rMsg = reaction.message;
    
                                                        translink.roles.addAndGetRouteRole(rMsg, rMsg.channel.guild, (err, res) => {
                                                            if (!err) {
                                                                var userGM = msg.guild.members.cache.find(gm => gm.id == user.id);
                                                                userGM.roles.add(res.role, "User wanted to be alerted to this specific route role.");
    
                                                                console.log(`Added guild member ${userGM.id} at guild ${userGM.guild.id} with role ${res.role.id}`);
                                                                
                                                                //check if route text channel exists
                                                                if (translink.channels.hasRouteTextChannel(msg.guild, res.role) == false) {
                                                                    //route text channel doesnt exist
                                                                    translink.channels.createRouteTextChannel(msg.guild, res.role, (err, res) => {
                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                            else {
                                                                console.log(res);
                                                            }
                                                        });
                                                    });
                                                }

                                            }).catch(e => console.log(e));
                                        }
                                    }

                                });
                            });

                        }
                        else {
                            console.log("No new mail messages to process.");
                        }

                        console.log("Closing IMAP connection.");
                        connection.end();
                    });
                });
            }).then(() => {
                console.log("Removing older alert messages.");
                imaps.connect(translink.imap.config).then(conn => {
                    translink.imap.deleteReadMessages(conn, (err, numOfDeletedMsgs) => {
                        if (err) {
                            console.log(err);
                        }

                        console.log(`Deleted ${numOfDeletedMsgs} message(s).`);

                        conn.end();
                    });
                });
            });

        }, null, true, process.env.TZ);
    },
    EveryMinuteJobs: (_client) => {
        return new CronJob('00 * * * * *', () => {

            //remove route channel if nobody has the role equipped. Remove the role afterwards.

            var allGuilds = _client.guilds.cache;
            allGuilds.forEach(g => {
                var groles = g.roles.cache;
                groles.forEach(rl => {

                    //check if role is a route role
                    var routeNames = translink.routeNames;
                    if (routeNames.includes(rl.name)) {

                        // return first gm with the route role, meaning atleast one has this role
                        var aGuildMemberHasRole = g.members.cache.find(gm => gm.roles.cache.find(r => r == rl));
                        
                        if (aGuildMemberHasRole == undefined) {
                            //nobody has this role
                            console.log(`Detected nobody using a route role ${rl.name} ${rl.id} in guild ${g.id}`);

                            //check if route channel exists
                            var categoryChnl = g.channels.cache.find(ch => ch.type == 'category' && ch.name.includes("Translink Alert"));
                            var routeChnl = g.channels.cache.find(ch => ch.name.includes(`${rl.name}_route_alerts`) && ch.parentID == categoryChnl.id);

                            //delete route channel
                            if (routeChnl != undefined) {
                                console.log(`Deleted route channel ${routeChnl.id} in guild ${g.id} as nobody has the role`);
                                routeChnl.delete(`Nobody has a role relating to these route alerts. Cleaning up channel.`);

                                //delete role
                                console.log(`Deleted route role ${rl.id} in guild ${g.id} as nobody has the role with the route channel being removed already.`);
                                rl.delete(`Nobody has this role to follow a route alert channel. Cleaning up role.`);
                            }
                        }
                    }
                });
            });

        }, null, true, process.env.TZ);
    }
}