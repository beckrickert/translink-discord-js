var mysql_promise = require('promise-mysql');

var pool = mysql_promise.createPool({
	host     : process.env.MYSQL_HOST,
	user     : process.env.MYSQL_USER,
	password : process.env.MYSQL_PASSWORD,
	port	 : process.env.MYSQL_PORT,
	database : process.env.MYSQL_DATABASE,
	connectionLimit: 10
});

var _getAConn = () => {
	return new Promise((resolve, reject) => {
		pool.then((p) => {
			p.getConnection().then((connection) => {
				resolve(connection);
			});
		});
	});
}

module.exports.pool = pool;
module.exports.getConn = _getAConn;