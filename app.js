var path = require('path');
require('dotenv').config({
    path: path.join('./', 'master.env')
});
var Discord = require('discord.js');
var express = require('express');
var randomstring = require('randomstring');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var momenttz = require('moment-timezone');
var fs = require('fs');
var utils = require('./utils.js');
var cronJobs = require('./cronJobs.js');

var prefix = process.env.PREFIX;

console.log(`App initializing.`);

//EXPRESS
var app = express();

app.disable('x-powered-by');
//app.set('trust proxy', 1);

//express routes
var router_index = require('./routes/index.js');
const translink = require('./translink.js');

//express uses
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use('/', router_index);
//for hosting html/css/js files
app.use('/', express.static('public'));

app.listen(process.env.HOST_PORT);
console.log(`Express initialized.`);

//DISCORD
var client = new Discord.Client({
    ws: {
        intents: Discord.Intents.ALL
    }
});

client.commands = new Discord.Collection();
var cooldowns = new Discord.Collection();
var commandFiles = fs.readdirSync('./commands').filter(f => f.endsWith('.js'));
for (var file of commandFiles) {
    var command = require(`./commands/${file}`);

    // set a new item in the Collection
    // with the key as the command name and the value as the exported module
    client.commands.set(command.name, command);
}

client.once('ready', () => {
    console.log("Discord is ready.");
    client.user.setActivity(
        "https://translink.ca", {
        type: "WATCHING"
    });

    //ensure text category exists for each guild, bot must have administrator privs
    var allGuilds = client.guilds.cache;
    
    allGuilds.forEach(g => {
        if (g.available) {
            console.log(`Guild ${g.id} available`);
            if (translink.channels.hasTranslinkTextCategory(g) == false) {
                console.log(`Guild ${g.id} does not have translink category channel`);
                //create category channel
                translink.channels.createRootCategory(g, (err, ch) => {
                    if (!err) {
                        console.log(`Guild ${g.id} category channel created ${ch.id}`);
                        //check unfiltered_alerts channel under this category
                        if (translink.channels.channelExistsUnderCategory(g, "unfiltered_alerts", "text", ch.id) == false) {
                            //create unfiltered_alerts channel under this category
                            g.channels.create("unfiltered_alerts", {
                                type: 'text',
                                topic: 'All Translink alerts with no specific route channel set.',
                                nsfw: false,
                                parent: ch.id,
                                reason: 'Mandatory channel for Bloofare bot.',
                                permissionOverwrites: [
                                    {
                                        id: g.roles.cache.find(rl => rl.name == "@everyone").id,
                                        type: "role",
                                        allow: 66560,
                                        deny: 871824209
                                    },
                                    {
                                        id: g.roles.cache.find(rl => rl.name.toUpperCase().includes("BLOOFARE")).id,
                                        type: "role",
                                        allow: 268692560,
                                        deny: 0
                                    }
                                ]
                            });
                        }

                    }
                    else {
                        console.log(err);
                    }
                });
            }
        }
        else {
            console.log(`guild ${g.id} not available`);
        }
    });
});

client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    var args = message.content.slice(prefix.length).split(/ +/);
    var commandName = args.shift().toLowerCase();

    //command name or its aliases
    var command = client.commands.get(commandName)
        || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
    
    //continue like nothing happened if no command found
    if (!command) return;

    //if command is guild (server) only
    if (command.guildOnly && message.channel.type !== 'text') {
        return message.reply('I can\'t execute that command inside DMs!');
    }

    //if arguments needed for command
    if (command.args && !args.length) {
        utils.removeAuthorMsg(message);
        let reply = `You didn't provide any arguments, ${message.author}!`;
        //provides if any, usage example
        if (command.usage) {
            reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
        }
        
        return message.channel.send(reply);
    }

    //cooldowns, ratelimiting of commands
    if (!cooldowns.has(command.name)) {
        cooldowns.set(command.name, new Discord.Collection());
    }

    var now = Date.now();
	var timestamps = cooldowns.get(command.name);
    var cooldownAmount = (command.cooldown || 3) * 1000;
    
    if (timestamps.has(message.author.id)) {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
            utils.removeAuthorMsg(message);
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
		}
    }
    
    timestamps.set(message.author.id, now);
	setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);


    try {
        command.execute(message, args);
    } catch (error) {
        console.log(error);
        message.reply('there was an error trying to execute that command!');
    }
});

//on message reaction added
client.on('messageReactionAdd', (messageReaction, user) => {
    
});

client.login(process.env.DISCORD_TOKEN)
.then(() => {
    console.log("Discord bot logging in.");
});

//CRON JOBS
console.log("Starting Cron Jobs.");

console.log("Waiting for database to be ready for connections.");
// Wait for Database to be ready for connections
setTimeout(() => {
    console.log("Attempting first time database connection");
    try {
    cronJobs.DailyMidnightJobs(client).start();
    cronJobs.DayOfMonth16Jobs(client).start();
    cronJobs.EveryFiveMinutesJobs(client).start();
    cronJobs.EveryMinuteJobs(client).start();
    }
    catch (err) {
        console.log("Error while attempting first time database connection. Database may not be ready yet.");
    }
}, 20000);

module.exports.Discordclient = client;