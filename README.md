# translink-discord-js

A NodeJS Discord bot wrapper for the Translink REST API.

## Requirements

Requires a [Translink API](https://developer.translink.ca/) token for routes and scheduling.

Requires a [Discord Token](https://discord.com/developers/applications) for Discord access and assigning bot to a server.

Requires an external configured IMAP-enabled email account for [Translink Alerts](https://www.translink.ca/alerts). It's recommended to allow all alerts for full service coverage.

Requires a SQL database with a configured user and database name (See `.env-template`) 

Supplied Docker-Compose file accompanies the bot with a MariaDB server.

## Installation

Copy the `.env-template` file to same directory named `master.env` and fill in your applicable environment variables. 

```bash
HOST_PORT=                    Internal container port used by the bot.
SUBHOST_NAME=                 Subdomain name if applicable for selfhosting message assets.
HOST_NAME=                    Top level domain name (or Public IP) for the bot required for selfhosting message assets.
TZ=                           Time Zone for all Docker containers and SQL server.
DISCORD_TOKEN=                Your Discord token here.
PREFIX=                       Command prefix. Eg. !translink
TRANSLINK_TOKEN=              Your Translink token here.
TRANSLINK_BASEURL=https://api.translink.ca/rttiapi/v1
TRANSLINK_UPASS_URL=https://upass.translink.ca
OWNER_TAG=                    Your Discord tag ownership for permissions. (To Do) Eg. Example#1234
MYSQL_HOST=bloofaredb         SQL server hostname or IP Address reachable by the Bot container.
MYSQL_USER=bloofare_user      SQL user bot username.
MYSQL_PASSWORD=               SQL user password.
MYSQL_ROOT_PASSWORD=          SQL user `root` password.
MYSQL_DATABASE=Bloofare_DB    SQL Database to use.
MYSQL_PORT=3306               SQL server port to use.
IMAP_USER=                    IMAP username. (depending on provider may be full email address)
IMAP_PASSWORD=                IMAP user password.
IMAP_HOST=                    IMAP provider server.
IMAP_PORT=                    IMAP provider port.
IMAP_INBOX_TRANSLINK=         IMAP mailbox/folder name where Translink alerts reside.
```

## Tips

If you decide to use a different hostname or port please update the `docker-compose` file accordingly.
First time setup of the bot may have a failed SQL connection, a restart is a quick solution. Upass functionality will not work without doing so.
It is highly recommended to place all email alerts from `*@alerts.translink.com` in a separate email folder mailbox.

## Usage

`help` Lists all of the bot's available commands.

`translink or (bloofare, bf)` DMs you a message about the bot.

`alert` Only useable in a server. Alerts you of the specified route in a separate text channel and role.

`nb [stop number] [route number]` Retrieves the next 3 buses for the specified route that reach this stop in all bounding directions up to a few hours.

`routes or (buses) [stop number]` Lists all routes for the specified stop number.

`upass or (multipass) option[add, remind, remindme, delete, remove, del, rm] date[early (default), end, last](optional)` DMs you a reminder either 16th or last day of every month to renew your multipass. Eg. `!upass add last` will remind you on the last day of the current month for next month's renewal.

## License
[MIT](https://choosealicense.com/licenses/mit/)
