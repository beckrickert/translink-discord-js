var express = require('express');
var router = express.Router();
var path = require('path');

router.get('/', (req, res) => {
    res.json({
        code: 1,
        message: 'Nothing to see here. Check back later.'
    });
});


module.exports = router;
