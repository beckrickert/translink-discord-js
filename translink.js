var request = require('request');
var _ = require("underscore");
var __ = require('lodash');
var _mysql = require('./mysqlconfig.js');
var conn = _mysql.getConn();
var momenttz = require('moment-timezone');
var urljoin = require('url-join');
var gMapsBaseURL = "https://google.com/maps/@";
var Discord = require('discord.js');
var rssParser = require('rss-parser');
var imaps = require('imap-simple');
var fs = require('fs');
var path = require('path');
const { resolve } = require('path');

var createGMapsCoordUrl = (lat, long, zoom = "18.5") => {
    lat += "";
    long += "";
    zoom  += "";
    lat = lat.replace(/[^0-9.-]/g, "");
    long = long.replace(/[^0-9.-]/g, "");
    zoom = zoom.replace(/[^0-9.]/g, "");

    var url = `${gMapsBaseURL}${lat},${long},${zoom}z`;
    return url;
}
var colors = [
    "#1D3557", 
    "#457B9D", 
    "#294D6D", 
    "#152B49", 
    "#5388A7", 
    "#465D79"
];

var logoPath = "";
if (process.env.SUBHOST_NAME.length != 0) {
    logoPath = `https://${process.env.SUBHOST_NAME}.${process.env.HOST_NAME}/img/translink.png`;
}
else {
    logoPath = `https://${process.env.HOST_NAME}/img/translink.png`;
}

var routeTitles = fs.readFileSync(path.join(__dirname, 'assets', 'translink_routes.csv'), 'utf8');
var routeNames = routeTitles.toString().split(/\r?\n/).filter(s => s != "");

module.exports = {
    channels: {
        hasRouteTextChannel(guild, routeRole) {
            //check if route text channel exists in guild
            var routeName = routeRole.name;
            var gChnls = guild.channels.cache;
    
            if (guild.available) {
                //check if category exists
                console.log(`Checking category channel for guild ${guild.id} exists`);
                var categoryChnl = this.hasTranslinkTextCategory(guild);

                if (categoryChnl) {
                    //check if route text channel exists under the category
                    console.log(`Checking if route channel for guild ${guild.id} exists for route ${routeName.name}`);
                    if (gChnls.some(ch => ch.name.includes(routeName) && ch.parentID == categoryChnl.id) == false) {
                        //text channel doesnt exist
                        return false;
                    }
                    else {
                        //text channel exists
                        return true;
                    }
                }
                else {
                    //create category channel
                    this.createRootCategory(guild);
                    return false;
                }
            }
        },
        createRouteTextChannel(guild, routeRole, callback) {
            var everyoneRole = guild.roles.cache.find(rl => rl.name == "@everyone");
            var botRole = guild.roles.cache.find(rl => rl.name.toUpperCase().includes("BLOOFARE"));
            var chnlName = `${routeRole.name}_route_alerts`;
            var categoryChnl = guild.channels.cache.find(ch => ch.type == 'category' && ch.name.includes("Translink Alerts"));
            guild.channels.create(chnlName, {
                type: 'text',
                topic: `All alerts relating for route ${routeRole.name}.`,
                nsfw: false,
                parent: categoryChnl,
                permissionOverwrites: [
                    {
                        id: everyoneRole.id,
                        type: "role",
                        allow: 0,
                        deny: 805829713
                    },
                    {
                        id: routeRole.id,
                        type: "role",
                        allow: 66624,
                        deny: 805763089
                    },
                    {
                        id: botRole.id,
                        type: "role",
                        allow: 268692560,
                        deny: 0
                    }
                ]
            }).then(ch => {
                console.log(`Created route text channel ${ch.id} for route role ${routeRole.id}`);
                ch.send(`<@&${routeRole.id}> All alerts relating to route ${routeRole.name} will be posted here.`);
                return callback(false, ch);
            }).catch(e => {
                return callback(e, null);
            });
        },
        hasTranslinkTextCategory(guild) {
            var chnls = guild.channels.cache;
            return chnls.some(ch => ch.type == "category" && ch.name == "Translink Alerts");
        },
        createRootCategory(guild, callback) {
            var everyoneRole = guild.roles.cache.find(rl => rl.name == "@everyone");
            guild.channels.create("Translink Alerts", {
                type: 'category',
                topic: 'Category for all Translink related channels.',
                nsfw: false,
                reason: 'Mandatory for Bloofare bot.',
                permissionOverwrites: [
                    {
                        id: everyoneRole.id,
                        type: "role",
                        allow: 66560,
                        deny: 871824209
                    }
                ]
            }).then(ch => {
                return callback(false, ch);
            }).catch(err => {
                return callback(err, null);
            });
        },
        channelExistsUnderCategory(guild, chName, type = 'text', chCategoryID = null) {
            var chnls = guild.channels.cache;
            return chnls.some(ch => ch.type == type && ch.name.includes(chName) && ch.parentID == chCategoryID);
        }
    },
    roles: {
        addAndGetRouteRole(msg, guild, callback) {
            if (guild.available) {
                //role name number by embed title name
                var msgTitle = msg.embeds[0].title.split(' ')[0];
                var isRouteMsg = routeNames.includes(msgTitle);
    
                var groles = guild.roles.cache;
                var roleName = msgTitle;
    
                //check message was only a route alert
                if (isRouteMsg) {
                    if (groles.some(r => r.name.includes(roleName)) == false) {

                        //add route role if not existing
                        guild.roles.create({
                            data: {
                                name: roleName,
                                color: colors[_.sample(colors)],
                                permissions: 68672,
                                mentionable: true
                            },
                            reason: "Automated creation because a user wanted to be alerted to this specific route."
                        }).then(rl => {

                            console.log(`Route role ${rl.id} created at guild ${guild.id}`);

                            return callback(false, {
                                role: rl
                            });
                        });
                    }
                    else {
    
                        //return role of that name
                        callback(
                            false,
                            {
                                role: groles.find(r => r.name.includes(roleName))
                            }
                        );
                    }
                }
                else {
                    callback(true, `Message was not about a specific route alert. Message id: ${msg.id}`);
                }
            }
            else {
                callback(true, `Guild ${guild.id} is not available.`);
            }
        }
    },
    stop(stopNo, callback) {
        stopNo += "";
        stopNo = stopNo.toString().replace(/[^0-9]+/, '');
        var options = {
            url: urljoin(process.env.TRANSLINK_BASEURL, "stops", stopNo, `?apikey=${process.env.TRANSLINK_TOKEN}`),
            headers: {
                accept: "application/json"
            }
        }

        request.get(options, (req, res) => {
            if (typeof callback == 'function') {
                var result = JSON.parse(res.body);

                //no error
                if (!result.Code) {
                    //string everything and trim
                    for (var key in result) {
                        if (typeof(key).includes("number", "string")) {
                            result[key] = result[key] + "";
                            result[key] = result[key].trim();
                        }
                    }

                    if (result.WheelchairAccess == 1) {
                        result.WheelchairAccess = true;
                    }
                    else {
                        result.WheelchairAccess = false;
                    }

                    callback({
                        stop: result
                    });
                }
                else {
                    //errored
                    callback({
                        error: result
                    });
                }
            }
        });
    },

    nextBus(stopNo, routeNo, callback) {
        stopNo += "";
        stopNo = stopNo.toString().replace(/[^0-9]+/, '');

        routeNo += "";
        routeNo = routeNo.padStart(3, '0');

        var options = {
            url: urljoin(process.env.TRANSLINK_BASEURL, "stops", stopNo, `estimates?apikey=${process.env.TRANSLINK_TOKEN}&count=3&timeframe=480&routeNo=${routeNo}`),
            headers: {
                accept: "application/json"
            }
        }

        request.get(options, (req, res) => {
            console.log(options);
            console.log(res);
            if (typeof callback == 'function') {
                var result = JSON.parse(res.body);
                if (!result.Code) {
                    if (result.length == 0) {
                        callback({
                            error: {
                                Code: 3005,
                                Message: "No stop estimates for the next 8 hours."
                            }
                        });
                    }
                    else {
                        var sch = [];
                        result.forEach(r => {
                            r.Schedules.forEach(s => {
                                var status;
                                switch (s.ScheduleStatus) {
                                    case "*":
                                        status = "SCHEDULED";
                                        break;
                                    case "-":
                                        status = "DELAYED";
                                        break;
                                    case "+":
                                        status = "EARLY";
                                        break;
                                    default:
                                        status = "";
                                        break;
                                }
                                sch.push({
                                    to: s.Destination.trim(),
                                    time: momenttz(s.ExpectedLeaveTime.trim(), "h:mma YYYY-MM-DD").format("h:mm A"),
                                    status: status,
                                    routeCancelled: s.CancelledTrip,
                                    stopCancelled: s.CancelledStop
                                });
                            });

                        });

                        callback({
                            routeName: result[0].RouteName,
                            routeNo: result[0].RouteNo,
                            schedules: sch
                        });
                        
                    }
                }
                else {
                    callback({
                        error: result
                    });
                }
            }
        })

    },

    routes(stopNo, callback) {
        stopNo += "";
        stopNo = stopNo.toString().replace(/[^0-9]+/, '');

        var options = {
            url: urljoin(process.env.TRANSLINK_BASEURL, "stops", stopNo, `?apikey=${process.env.TRANSLINK_TOKEN}`),
            headers: {
                accept: "application/json"
            }
        }

        request.get(options, (req, res) => {
            if (typeof callback == 'function') {
                var result = JSON.parse(res.body);
                if (!result.Code) {
                    if (result.length == 0) {
                        callback({
                            error: {
                                Code: 3005,
                                Message: "No stop estimates found for the next 8 hours."
                            }
                        });
                    }
                    else {
                        callback({
                            gMaps: createGMapsCoordUrl(result.Latitude, result.Longitude),
                            stopName: result.Name.trim(),
                            buses: result.Routes
                        });
                    }
                }
                else {
                    callback({
                        error: result
                    });
                }
            }
        })

    },

    upass: {
        check(uid) {
            return new Promise((resolve, reject) => {
                //check if a reminder exists, boolean
                var connection;
                conn.then(conn => {
                    connection = conn;
                    var select_sql = "SELECT * FROM upassReminders WHERE uid = ? LIMIT 1";
                    return connection.query(select_sql, [uid]);
                }).then((res) => {
                    if (res[0] != undefined) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                }).catch((err) => {
                    console.log(err);
                    reject(true);
                });
            });
        },
        update(uid, newFreq, callback) {
            var connection;

            newFreq = newFreq.toUpperCase();
            switch (newFreq) {
                case "EARLY":
                    newFreq = "16THOFMONTH";
                    break;
                case "LAST":
                    newFreq = "LASTDAYOFMONTH";
                    break;
            }

            conn.then(conn => {
                connection = conn;
                var update_sql = "UPDATE upassReminders SET reminder_freq = ?, created_time = ? WHERE uid = ? LIMIT 1";
                return connection.query(update_sql, [newFreq, momenttz().tz(process.env.TZ).utc().format("YYYY-MM-DD HH:mm:ss"), uid]);
            }).then((r) => {
                if (r.changedRows != 0) {
                    callback(false, true);
                }
                else {
                    callback("UPass Reminder not updated due to error. Please try again.", false);
                }
            }).catch((err) => {
                callback(err, false);
            });
        },
        add(uid, newFreq, callback) {
            var connection;

            newFreq = newFreq.toUpperCase();
            switch (newFreq) {
                case "EARLY":
                    newFreq = "16THOFMONTH";
                    break;
                case "LAST":
                    newFreq = "LASTDAYOFMONTH";
                    break;
            }

            conn.then(conn => {
                connection = conn;
                var insert_sql = "INSERT INTO upassReminders (uid, reminder_freq) VALUES (?, ?)";
                return connection.query(insert_sql, [uid, newFreq]);
            }).then(r => {
                if (r.affectedRows != 0) {
                    callback(false, true);
                }
                else {
                    callback("UPass Reminder not added due to error. Please try again.", false);
                }
            }).catch((err) => {
                callback(err, false);
            });
        },
        remove(uid, callback) {
            var connection;

            conn.then(conn => {
                connection = conn;
                var rm_sql = "DELETE FROM upassReminders WHERE uid = ? LIMIT 1";
                return connection.query(rm_sql, [uid]);
            }).then(r => {
                if (r.affectedRows != 0) {
                    callback(false, true);
                }
                else {
                    callback("UPass Reminder not removed due to error. Please try again.", false);
                }
            }).catch((err) => {
                callback(err, false);
            });
        },
        triggerReminders(_client, freq, callback) {
            var msg = [];
            msg.push("This is a friendly reminder to renew your UPass for next month! " + `${process.env.TRANSLINK_UPASS_URL}`);
            msg.push(`*This was configured by you to be reminded ${(freq.toLowerCase() == "early") ? "for every 16th of each month." : " last day of each month."}*`);
            msg.push("*If you would like to remove or change these reminders please send me the applicable parameter option for the `upass` command. See the parameters with `!help upass`*");
            msg = msg.join("\n").trim().replace(/ +/g, " ");

            switch (freq.toLowerCase()) {
                case "early":
                    freq = "16THOFMONTH"; 
                    break;
                case "last":
                    freq = "LASTDAYOFMONTH"; 
                    break;
                default:
                    freq = "16THOFMONTH"; 
                    break;
            }

            var connection;
            var select_sql = "SELECT * FROM upassReminders WHERE reminder_freq = ?";
            var update_sql = "UPDATE upassReminders SET last_reminded = ? WHERE uid = ? LIMIT 1";
            conn.then(conn => {
                connection = conn;
                return connection.query(select_sql, [freq]);
            }).then((rows) => {
                rows.forEach((r) => {
                    var now_time_UTC = momenttz().tz(process.env.TZ).utc().format("YYYY-MM-DD HH:mm:ss");
                    var uid = r.uid;

                    //send dm
                    _client.users.fetch(uid)
                    .then(u => {
                        return u.createDM()
                    }).then(c => {
                        c.send(msg, {split: true});
                    }).catch(err => {
                        console.log(err);
                        callback(err, false);
                    });

                    //update last_reminded timestamp
                    connection.query(update_sql, [now_time_UTC, uid])
                    .catch((err) => {
                        console.log(err);
                        callback(err, false);
                    });
                });

                callback(false, true);
            });
        }
    },

    imap: {
        processMessages(messages) {
            var embeds = [];
            var formattedMessages = [];
            messages.forEach(message => {
                var headerPart = __.find(message.parts, {"which": "HEADER"});
                var textPart = __.find(message.parts, {"which": "TEXT"});

                var contentType = headerPart.body["content-type"][0];
                contentType = contentType.split("; ").map(item => {
                    item = item.trim();
                    //array-ize directives eg. charset=utf-8
                    item = item.includes("=") && (!item.startsWith("=") && !item.endsWith("=")) ? item.split("=\""): item;

                    //if item is split into more than 2 parts
                    if (Array.isArray(item) && item.length > 2) {
                        var tempItem = item;
                        item = [];
                        item.push(tempItem[0]);
                        item.push(tempItem.slice(1).join(""));
                    }

                    //remove ending quotes
                    if (!Array.isArray(item) && (item.startsWith(("'", '"')) && item.endsWith(("'", '"'))) ) {
                        item = item.substring(1, item.length - 1);
                    }

                    //clean up orphaned quotes in boundary


                    //remove ending quotes from array-ized directives key([0]) & value([1])
                    if (Array.isArray(item)) {
                        item = item.map(x => {
                            if (x.startsWith(("'", '"')) && x.endsWith(("'", '"'))) {
                                return x.substring(1, x.length - 1);
                            }
                            else if (x.startsWith(("'", '"'))) {
                                return x.substring(1);
                            }
                            else if (x.endsWith(("'", '"'))) {
                                return x.substring(0, x.length - 1);
                            }
                            else {
                                return x;
                            }
                        });
                    }

                    return item;
                });

                var messageBody = Buffer.from(textPart.body).toString("ascii");

                //plaintext email body
                if (contentType.find((n) => {
                    if (!Array.isArray(n)) {
                        return n.toLowerCase() == "text/plain";
                    }
                })) {
                    var charset = contentType.find((x) => {
                        if (Array.isArray(x)) {
                            if (x[0].toLowerCase() == "charset") {
                                return x[1].toLowerCase();
                            }
                        }
                    });

                    messageBody = Buffer.from(messageBody, charset).toString("utf8");
                    messageBody = messageBody.replace(/\r\n/g, "\n");
                    
                    console.log(messageBody);
                }

                //multipart eg. plaintext and html
                if (contentType.find(n => n.toLowerCase().startsWith("multipart/"))) {
                    var boundary = contentType.find((x) => {
                        if (Array.isArray(x)) {
                            if (x[0].toLowerCase() == "boundary") {
                                return true;
                            }
                        }
                    });

                    if (boundary[0].toLowerCase() == "boundary") {
                        boundary.shift();
                        boundary = boundary[0];
                    }

                    messageBody = messageBody.split(`--${boundary}`);
                    messageBody.shift();
                }

                //clean up multipart messagebody
                if (Array.isArray(messageBody)) {
                    var formattedSegments = [];
                    
                    messageBody.forEach(segment => {
                        segment = segment.trim();
                        segment =  segment.split("\r\n");

                        //clean up continued "dashed" lines.
                        segment = segment.map((line, index) => {
                            if (line.endsWith("=") && segment[index + 1].length != 0) {
                                line = line.substring(0, line.length-1);
                                line += "\n";

                                return line;
                            }
                            else {
                                return line;
                            }
                        });

                        var tempSegment = [];
                        //merge \n ending lines with next line
                        segment.forEach((line, index) => {
                            if (line.endsWith("\n")) {
                                line = line.substring(0, line.length-1);

                                line += segment[index+1];
                                segment.splice(index+1, 1);

                                tempSegment.push(line);
                            }
                            else {
                                tempSegment.push(line);
                            }

                            if (tempSegment.filter(s => s.endsWith("\n")).length != 0) {
                                tempSegment = tempSegment.map((l, i) => {
                                    if (l.endsWith("\n")) {
                                        l = l.substring(0, l.length - 1);
                                        l += segment[i+1];
                                        tempSegment[i] = l;
                                        segment.splice(i+1, 1);
                                        return l;
                                    }
                                    else {
                                        return l;
                                    }
                                });
                            }
                        });

                        //remove repeating whitespace characters
                        segment = tempSegment.map(s => {
                            return s.replace(/\s+/g, " ");
                        });

                        //remove disable alert line
                        segment = segment.filter((s) => {
                            return s.toLowerCase().includes("disable alerts, follow this link") ? false : true;
                        });

                        //remove message body headers near beginning.
                        segment.slice(0, 4).forEach((l, i) => {
                            if (l.startsWith("Content-Type: ") || l.startsWith("Content-Transfer-Encoding: ") || l.startsWith("Content-Disposition: ") || l.startsWith("Content-ID: ")) {
                                segment.shift();
                            }
                        });

                        //utf8 encoding characters
                        segment = segment.map(s => {
                            if (s.includes(" =E2=80=93 ")) {
                                s = s.replace(/=E2=80=93/g, "-");
                                return s;
                            }
                            else {
                                return s;
                            }
                        });

                        //remove empty first and last lines
                        if (segment[0].length == 0) {
                            segment.shift();
                        }
                        if (segment[segment.length-1].length == 0) {
                            segment.pop();
                        }

                        formattedSegments.push(segment);
                    });

                    formattedSegments.forEach((s, i) => {
                        if (s.includes("--") && s.length == 1) {
                            formattedSegments.splice(i, 1);
                        }
                    });

                    messageBody = formattedSegments;
                }

                messageBody.subject = headerPart.body.subject[0];
                messageBody.date = momenttz.tz(headerPart.body.date[0], 'America/Vancouver').toDate();
                messageBody.from = headerPart.body.from[0];

                formattedMessages.push(messageBody);
            });

            formattedMessages.forEach(m => {
                var mergedBodies = "";
                if (Object.keys(m).filter(e => !isNaN(e)).length > 1) {
                    mergedBodies = Object.entries(m).map((e) => {
                        if (Array.isArray(e) && !isNaN(e[0])) {
                            return e[1].join("\n");
                        }
                        else {
                            return undefined;
                        }
                    }).filter(e => e != undefined);
                }
                else {
                     mergedBodies = m.find(x => Array.isArray(x));
                }

                var msgEmbed = new Discord.MessageEmbed()
                    .setColor(_.sample(colors))
                    .setAuthor("Translink Alerts", logoPath, "https://alerts.translink.ca/")
                    .setTitle(m.subject)
                    .setDescription(mergedBodies.join("\n"))
                    .setFooter("Posted")
                    .setTimestamp(m.date);

                embeds.push(msgEmbed);
            });

            return embeds;
        },
        deleteReadMessages(conn, cb) {

            var searchCriteria = [
                'SEEN',
                ['FROM', 'donotreply@alerts.translink.ca']
            ];
            
            var fetchOptions = {
                bodies: ['HEADER'],
                markSeen: false
            }

            
            conn.openBox(this.inbox).then(() => {
                conn.search(searchCriteria, fetchOptions)
                .then(messages => {
                    if (messages.length != 0) {
                        var uidsToDelete = messages.filter(m => {
                            var h = m.parts.filter(p => p.which == 'HEADER')[0].body;
                            if (h.to[0] == process.env.IMAP_USER || h.from[0].includes('translink.ca')) {
                                return true;
                            }
                            else {
                                false;
                            }
                        }).map(m => m.attributes.uid);
    
                        conn.deleteMessage(uidsToDelete);
    
                        cb(false, uidsToDelete.length);
                    }
                    else {
                        cb(false, 0);
                    }
                }).catch(err => {
                    cb(err, false);
                });
            }).catch(err => {
                cb(err, false);
            });;

        },
        config: {
            imap: {
                user: process.env.IMAP_USER,
                password: process.env.IMAP_PASSWORD,
                host: process.env.IMAP_HOST,
                port: process.env.IMAP_PORT,
                tls: true,
                authTimeout: 3000
            }
        },
        inbox: process.env.IMAP_INBOX_TRANSLINK
    },

    routeNames: routeNames,

    colors: colors
}


